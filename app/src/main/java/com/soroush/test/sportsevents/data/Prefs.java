package com.soroush.test.sportsevents.data;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.Nullable;

import static com.soroush.test.sportsevents.helper.Constants.NOT_INSTANTIATED;
import static com.soroush.test.sportsevents.helper.Constants.PREFS_NAME;

public class Prefs {
    public boolean getIsLoggedIn() {
        return prefs.getBoolean(PrefKeys.IsLoggedIn, false);
    }

    @Nullable
    public String getCurrentUserEmail() {
        return prefs.getString(PrefKeys.UserEmail, null);
    }

    public void setIsLoggedIn(boolean value) {
        prefs.edit().putBoolean(PrefKeys.IsLoggedIn, value).apply();
    }

    public void setCurrentUserEmail(String email) {
        prefs.edit().putString(PrefKeys.UserEmail, email).apply();
    }

    public static void instantiate(Context context) {
        instance = new Prefs(context);
    }

    public static Prefs getInstance() throws IllegalStateException {
        if (instance == null) throw new IllegalStateException(NOT_INSTANTIATED);
        return instance;
    }

    private Prefs(Context context) {
        prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }


    private class PrefKeys {
        private static final String IsLoggedIn = "prefKey_IsLoggedIn";
        private static final String UserEmail = "prefKey_UserEmail";
    }

    private static Prefs instance;
    private SharedPreferences prefs;
}
