package com.soroush.test.sportsevents.data.actions;

import com.soroush.test.sportsevents.data.Prefs;
import com.soroush.test.sportsevents.data.db.Db;
import com.soroush.test.sportsevents.data.model.User;
import com.soroush.test.sportsevents.viewmodel.viewinterface.UserView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class UserActons extends BaseAction {
    private UserView userView;

    public UserActons(UserView userView) {
        this.userView = userView;
    }

    public Disposable fetchUser() {
        String userEmail = Prefs.getInstance().getCurrentUserEmail();
        return Db.getInstance().userDao().getUser(userEmail)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        userList -> {
                            if (userList != null && !userList.isEmpty())
                                userView.setUser(userList.get(0));
                        },
                        this::logError
                );
    }

    public Disposable updateUserFavSport(User user, String favSport) {
        if (user != null) {
            user.favSport = favSport;
            return Db.getInstance().userDao().update(user)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            () -> {
                            },
                            this::logError
                    );
        }
        return null;
    }
}
