package com.soroush.test.sportsevents.viewmodel.viewinterface;

import com.soroush.test.sportsevents.data.actions.LoginActions;

public interface LoginView {
    void setAttemptStart();
    void setAttemptSucceeded(String userName);
    void setAttemptFailed(LoginActions.Error error);
}
