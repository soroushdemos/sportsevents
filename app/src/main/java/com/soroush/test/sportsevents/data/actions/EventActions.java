package com.soroush.test.sportsevents.data.actions;

import com.soroush.test.sportsevents.data.model.League;
import com.soroush.test.sportsevents.data.service.EventsService;
import com.soroush.test.sportsevents.viewmodel.viewinterface.EventView;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.soroush.test.sportsevents.helper.Constants.UPDATE_INTERVAL;

public class EventActions extends BaseAction {
    private EventView eventView;

    public EventActions(EventView eventView) {
        this.eventView = eventView;
    }

    public Disposable getEventsForLeague(int leagueId) {
        return EventsService.getInstance().getEventsByLeagueId(leagueId)
                .subscribe(
                        events -> eventView.setEvents(events),
                        this::logError
                );
    }

    public Disposable updateEventsPeriodically(League selectedLeague) {
        return Observable.interval(0, UPDATE_INTERVAL, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        (time) -> {
                            if (selectedLeague != null)
                                getEventsForLeague(selectedLeague.idLeague);
                        },
                        this::logError
                );
    }
}
