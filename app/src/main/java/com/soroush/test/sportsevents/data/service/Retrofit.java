package com.soroush.test.sportsevents.data.service;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

class Retrofit {
    /*********
     * Public
     *********/
    static retrofit2.Retrofit getClient(Context context) {
        return getInstance(context).retrofitClient;
    }

    /**********
     * Private
     **********/
    private static final int CACHE_SIZE = 10 * 1024 * 1024; // 10 MB
    private static final String BASE_URL = "https://www.thesportsdb.com/api/v1/json/1/";

    @Nullable
    private retrofit2.Retrofit retrofitClient;

    @Nullable
    private static Retrofit instance;

    private Retrofit(Context context) {
        Cache cache = new Cache(context.getCacheDir(), CACHE_SIZE);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cache(cache)
                .build();

        retrofit2.Retrofit.Builder builder = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

        this.retrofitClient = builder.build();
    }

    @NonNull
    private static Retrofit getInstance(Context context) {
        if (instance == null) instance = new Retrofit(context);
        return instance;
    }
}
