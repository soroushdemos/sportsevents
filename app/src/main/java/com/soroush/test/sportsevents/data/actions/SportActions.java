package com.soroush.test.sportsevents.data.actions;

import com.soroush.test.sportsevents.data.model.Sport;
import com.soroush.test.sportsevents.data.service.SportService;
import com.soroush.test.sportsevents.helper.Constants;
import com.soroush.test.sportsevents.viewmodel.viewinterface.SportView;

import java.util.ArrayList;

import io.reactivex.disposables.Disposable;

public class SportActions extends BaseAction {
    private SportView sportView;

    public SportActions(SportView sportView) {
        this.sportView = sportView;
    }

    public Disposable fetchSports() {
        return SportService.getInstance().getAllSports()
                .subscribe(
                        sports -> {
                            ArrayList<Sport> _sports = new ArrayList<>();
                            for (Sport sport : sports)
                                if (Constants.SUPPORTED_SPORT_NAMES.contains(sport.strSport))
                                    _sports.add(sport);

                            sportView.setSports(_sports);
                        },
                        this::logError
                );

    }
}
