package com.soroush.test.sportsevents.data.service;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.soroush.test.sportsevents.data.model.Team;
import com.soroush.test.sportsevents.helper.Constants;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class TeamService {
    public Observable<List<Team>> getTeamsByLeagueName(String leagueName) {
        return api.getTeamsByLeagueName(leagueName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(item -> item.teams);

    }

    @NonNull
    public static TeamService getInstance() throws IllegalStateException {
        if (instance == null) throw new IllegalStateException(Constants.NOT_INSTANTIATED);
        return instance;
    }

    static void instantiate(Context context) {
        instance = new TeamService(context);
    }

    private TeamService(Context context) {
        api = Retrofit.getClient(context).create(TeamApi.class);
    }

    @Nullable
    private static TeamService instance;
    private TeamApi api;
}


interface TeamApi {
    @GET("search_all_teams.php")
    Observable<TeamResponse> getTeamsByLeagueName(@Query("l") String leagueName);
}

class TeamResponse {
    List<Team> teams;
}