package com.soroush.test.sportsevents.data.service;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.soroush.test.sportsevents.data.model.League;
import com.soroush.test.sportsevents.helper.Constants;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class LeagueService {
    /********************
     * Public
     ********************/
    @NonNull
    public static LeagueService getInstance() throws IllegalStateException {
        if (instance == null) throw new IllegalStateException(Constants.NOT_INSTANTIATED);
        return instance;
    }

    public Observable<ArrayList<League>> getAllLeagues() {
        return api.getAllLeagues()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(leaguesResponse -> leaguesResponse.leagues);
    }


    public Observable<League> getLeagueDetail(int leagueId) {
        return api.getLeagueDetail(leagueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(leaguesResponse -> leaguesResponse.leagues.get(0));
    }
    /********************
     * Package Private
     ********************/
    static void instantiate(Context context) {
        if (instance == null) {
            instance = new LeagueService(context);
        }
    }

    private LeagueService(Context context) {
        api = Retrofit.getClient(context).create(LeagueApi.class);
    }

    /***********
     * Private
     ***********/
    @Nullable
    private static LeagueService instance;
    private LeagueApi api;
}

class LeaguesResponse {
    ArrayList<League> leagues;
}

interface LeagueApi {
    @GET("all_leagues.php")
    Observable<LeaguesResponse> getAllLeagues();

    @GET("lookupleague.php")
    Observable<LeaguesResponse> getLeagueDetail(@Query("id") int leagueId);
}
