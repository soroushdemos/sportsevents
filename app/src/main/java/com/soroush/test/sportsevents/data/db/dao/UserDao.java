package com.soroush.test.sportsevents.data.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.soroush.test.sportsevents.data.model.User;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user WHERE email=:email AND password=:password LIMIT 1")
    Flowable<List<User>> getUser(String email, String password);

    @Query("SELECT * FROM user WHERE email=:email LIMIT 1")
    Flowable<List<User>> getUser(String email);

    @Insert
    Completable insert(User users);

    @Update
    Completable update(User user);
}
