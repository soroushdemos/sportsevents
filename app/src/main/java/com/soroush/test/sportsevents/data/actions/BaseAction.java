package com.soroush.test.sportsevents.data.actions;

import android.util.Log;

class BaseAction {
    void logError(Throwable e) {
        Log.w(TAG, e.getMessage());
    }

    private static final String TAG = LeagueActions.class.getCanonicalName();
}
