package com.soroush.test.sportsevents.data.service;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.soroush.test.sportsevents.data.model.Event;
import com.soroush.test.sportsevents.helper.Constants;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class EventsService {
    public Observable<ArrayList<Event>> getEventsByLeagueId(int leagueId) {
        return api.getNextEventsByLeague(leagueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(eventsResponse -> eventsResponse.events != null ? eventsResponse.events : new ArrayList<>());
    }

    public Observable<ArrayList<Event>> getBaseByLeagueId(int leagueId) {
        return api.getPastEventsByLeague(leagueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(eventsResponse -> eventsResponse.events);
    }

    @NonNull
    public static EventsService getInstance() throws IllegalStateException {
        if (instance == null) throw new IllegalStateException(Constants.NOT_INSTANTIATED);
        return instance;
    }

    /*******************
     * Package Private
     *******************/
    static void instantiate(Context context) {
        instance = new EventsService(context);
    }

    private EventsService(Context context) {
        api = Retrofit.getClient(context).create(EventsApi.class);
    }

    @Nullable
    private static EventsService instance;
    private EventsApi api;
}

interface EventsApi {
    @GET("eventsnextleague.php")
    Observable<EventsResponse> getNextEventsByLeague(@Query("id") int leagueId);

    @GET("eventspastleague.php")
    Observable<EventsResponse> getPastEventsByLeague(@Query("id") int leagueId);
}

class EventsResponse {
    @Nullable
    ArrayList<Event> events;
}
