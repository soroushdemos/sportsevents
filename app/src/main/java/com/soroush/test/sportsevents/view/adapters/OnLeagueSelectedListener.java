package com.soroush.test.sportsevents.view.adapters;

import com.soroush.test.sportsevents.data.model.League;

public interface OnLeagueSelectedListener {
    void onLeagueSelected(League league);
}
