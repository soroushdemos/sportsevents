package com.soroush.test.sportsevents;

import android.app.Application;

import com.soroush.test.sportsevents.data.Prefs;
import com.soroush.test.sportsevents.data.db.Db;
import com.soroush.test.sportsevents.data.service.Services;

public class SportsEventsApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        instantiateSingletons();
    }

    private void instantiateSingletons() {
        Db.instantiate(getApplicationContext());
        Services.instantiate(getApplicationContext());
        Prefs.instantiate(getApplicationContext());
    }
}
