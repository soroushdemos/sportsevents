package com.soroush.test.sportsevents.view.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.soroush.test.sportsevents.R;
import com.soroush.test.sportsevents.data.model.League;
import com.soroush.test.sportsevents.data.model.Sport;
import com.soroush.test.sportsevents.view.adapters.EventFeedbackListener;
import com.soroush.test.sportsevents.view.adapters.EventRecyclerViewAdapter;
import com.soroush.test.sportsevents.view.adapters.LeagueRecyclerViewAdapter;
import com.soroush.test.sportsevents.view.adapters.OnLeagueSelectedListener;
import com.soroush.test.sportsevents.view.fragment.SportSelectionDialogFragment;
import com.soroush.test.sportsevents.view.fragment.SportSelectionListener;
import com.soroush.test.sportsevents.viewmodel.SportsEventsViewModel;
import com.squareup.picasso.Picasso;

import static com.soroush.test.sportsevents.helper.Constants.ACTIVITY_REQUEST_LOGIN;
import static com.soroush.test.sportsevents.helper.Constants.RESULT_SUCCESS;

public class MainActivity extends AppCompatActivity
        implements OnLeagueSelectedListener, SportSelectionListener, EventFeedbackListener {
    /*****************************
     * AppCompatActivity Methods
     *****************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewModel = ViewModelProviders.of(this).get(SportsEventsViewModel.class);
        setupViews();
        subscribeObservers();
        setupOnClickListeners();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ACTIVITY_REQUEST_LOGIN && resultCode == RESULT_SUCCESS) {
            viewModel.setUserLoggedIn(true);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /****************************
     * OnLeagueSelectedListener
     ****************************/
    @Override
    public void onLeagueSelected(League league) {
        viewModel.setSelectedLeague(league);
    }

    /*************************
     * SportSelectionListener
     *************************/
    @Override
    public void onSportSelected(Sport sport) {
        viewModel.setSelectedSport(sport);
    }

    /*************************
     * EventFeedbackListener
     *************************/
    @Override
    public void onVideoLinkClicked(String url) {
        startActivity(new Intent(
                Intent.ACTION_VIEW,
                Uri.parse(url)
        ));
    }

    /**********
     * Private
     **********/
    private SportsEventsViewModel viewModel;
    private LeagueRecyclerViewAdapter leagueRecyclerViewAdapter;
    private EventRecyclerViewAdapter eventRecyclerViewAdapter;

    private void setupViews() {
        leagueRecyclerViewAdapter = new LeagueRecyclerViewAdapter(this);
        eventRecyclerViewAdapter = new EventRecyclerViewAdapter(this);

        RecyclerView recyclerView_leagues = findViewById(R.id.recyclerView_leagues);
        recyclerView_leagues.setAdapter(leagueRecyclerViewAdapter);
        recyclerView_leagues.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        RecyclerView recyclerView_events = findViewById(R.id.recyclerView_events);
        recyclerView_events.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView_events.setAdapter(eventRecyclerViewAdapter);
    }

    private void subscribeObservers() {
        // user
        ImageView imageView_login = findViewById(R.id.imageView_login);
        viewModel.getIsUserLoggedIn().observe(this, isLoggedIn -> {
            imageView_login.setColorFilter(getResources().getColor(
                    isLoggedIn ? R.color.accentColor : R.color.primaryDark
            ));
            imageView_login.setAlpha(isLoggedIn ? 1 : 0.5f);
        });
        // sport
        ImageView imageView_sport = findViewById(R.id.imageView_sport_logo);
        TextView textView_sport = findViewById(R.id.textView_sport_title);

        View viewSplash = findViewById(R.id.layout_splash);
        viewModel.getSelectedSport().observe(this, sport -> {
            if (sport != null) {
                textView_sport.setText(String.format("⋮ %s", sport.strSport));
                if (sport.strSportThumb != null && !sport.strSportThumb.isEmpty())
                    Picasso.get()
                            .load(sport.strSportThumb)
                            .placeholder(R.drawable.ic_image)
                            .error(R.drawable.ic_image)
                            .fit()
                            .into(imageView_sport);
                viewSplash.setVisibility(View.GONE);
            } else {
                viewSplash.setVisibility(View.VISIBLE);
            }
        });

        // leagues
        RecyclerView recyclerView_leagues = findViewById(R.id.recyclerView_leagues);
        ProgressBar progressBar_leagues = findViewById(R.id.progress_leagues);
        View separator = findViewById(R.id.separator);

        viewModel.getLeagues().observe(this, leagues -> {
            if (leagues == null) {
                progressBar_leagues.setVisibility(View.VISIBLE);
                recyclerView_leagues.setVisibility(View.INVISIBLE);
                separator.setVisibility(View.INVISIBLE);
            } else {
                progressBar_leagues.setVisibility(View.GONE);
                recyclerView_leagues.setVisibility(View.VISIBLE);
                leagueRecyclerViewAdapter.setLeagues(leagues, viewModel.getSelectedLeague().getValue());
                separator.setVisibility(View.VISIBLE);
            }
        });

        viewModel.getSelectedLeague().observe(this, league ->
                leagueRecyclerViewAdapter.setLeagues(viewModel.getLeagues().getValue(), league));

        // events
        RecyclerView recyclerView_events = findViewById(R.id.recyclerView_events);
        ProgressBar progressBar_events = findViewById(R.id.progress_events);
        TextView textView_noEvents = findViewById(R.id.textView_noUpcomingEvents);

        viewModel.getEvents().observe(this, events -> {
            if (events == null) {
                recyclerView_events.setVisibility(View.INVISIBLE);
                progressBar_events.setVisibility(View.VISIBLE);
                textView_noEvents.setVisibility(View.INVISIBLE);
            } else if (events.size() > 0) {
                progressBar_events.setVisibility(View.GONE);
                recyclerView_events.setVisibility(View.VISIBLE);
                eventRecyclerViewAdapter.setData(events, viewModel.getTeamLogos().getValue());
                textView_noEvents.setVisibility(View.INVISIBLE);
            } else {
                progressBar_events.setVisibility(View.GONE);
                recyclerView_events.setVisibility(View.INVISIBLE);
                textView_noEvents.setVisibility(View.VISIBLE);

                String leagueName = getResources().getString(R.string.league);
                if (viewModel.getSelectedLeague().getValue() != null)
                    leagueName = viewModel.getSelectedLeague().getValue().strLeague;

                textView_noEvents.setText(String.format(
                        getResources().getString(R.string.no_upcoming_events),
                        leagueName
                ));
            }
        });

        viewModel.getTeamLogos().observe(this, logos ->
                eventRecyclerViewAdapter.setData(viewModel.getEvents().getValue(), logos));
    }

    private void setupOnClickListeners() {
        findViewById(R.id.textView_sport_title)
                .setOnClickListener(view -> {
                    SportSelectionDialogFragment.show(
                            getSupportFragmentManager(),
                            viewModel.getSports().getValue(),
                            viewModel.getSelectedSport().getValue());
                });

        findViewById(R.id.imageView_login).setOnClickListener(v -> {
            if (viewModel.getIsUserLoggedIn().getValue() != null) {
                boolean isLoggedIn = viewModel.getIsUserLoggedIn().getValue();
                if (isLoggedIn) showLogoutDialog();
                else LoginActivity.startForResult(this, ACTIVITY_REQUEST_LOGIN);
            }
        });
    }

    private void showLogoutDialog() {
        new AlertDialog.Builder(this)
                .setMessage(getResources().getString(R.string.logout_areyousure))
                .setPositiveButton(
                        getResources().getString(R.string.yes),
                        (dialog, which) -> {
                            viewModel.logOut();
                        }
                )
                .setNegativeButton(
                        getResources().getString(R.string.no),
                        (dialog, which) -> {
                            dialog.dismiss();
                        }
                )
                .show();
    }
}