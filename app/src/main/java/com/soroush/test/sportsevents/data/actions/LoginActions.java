package com.soroush.test.sportsevents.data.actions;

import androidx.annotation.NonNull;

import com.soroush.test.sportsevents.data.db.Db;
import com.soroush.test.sportsevents.data.model.User;
import com.soroush.test.sportsevents.helper.Constants;
import com.soroush.test.sportsevents.viewmodel.viewinterface.LoginView;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginActions {
    public enum AttemptStatus { STARTED, FAILED, SUCCEEDED }
    public enum Error { USER_ALREADY_EXISTS, WRONG_USERNAME_OR_PASSWORD, INTERNAL_ERROR }


    public LoginActions(LoginView loginView) {
        this.loginView = loginView;
    }

    public Disposable attemptLogin(@NonNull String userName, @NonNull String password) {
        loginView.setAttemptStart();
        return Db.getInstance().userDao().getUser(userName, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        userList -> {
                            if (userList != null && !userList.isEmpty()) {
                                loginView.setAttemptSucceeded(userName);
                            } else if (userList == null)
                                loginView.setAttemptFailed(Error.INTERNAL_ERROR);
                            else loginView.setAttemptFailed(Error.WRONG_USERNAME_OR_PASSWORD);
                        },
                        err -> loginView.setAttemptFailed(Error.INTERNAL_ERROR));
    }

    public Disposable attemptSignUp(@NonNull String userName, @NonNull String password) {
        if (userName.isEmpty())
            throw new IllegalArgumentException(String.format(Constants.CANNOT_BE_EMPTY, "userName"));
        if (password.isEmpty())
            throw new IllegalArgumentException(String.format(Constants.CANNOT_BE_EMPTY, "password"));

        loginView.setAttemptStart();
        return checkUserExists(userName)
                .subscribe(
                        userList -> {
                            if (userList != null && userList.isEmpty()) {
                                proceedToSignUp(userName, password);
                            } else if (userList == null) {
                                loginView.setAttemptFailed(Error.INTERNAL_ERROR);
                            } else {
                                loginView.setAttemptFailed(Error.USER_ALREADY_EXISTS);
                            }
                        },
                        err -> loginView.setAttemptFailed(Error.INTERNAL_ERROR));
    }


    /**********
     * Private
     **********/
    private Flowable<List<User>> checkUserExists(@NonNull String userName) {
        return Db.getInstance().userDao().getUser(userName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Disposable proceedToSignUp(@NonNull String userName, @NonNull String password) {
        return Db.getInstance().userDao().insert(new User(userName, password))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        () -> loginView.setAttemptSucceeded(userName),
                        err -> loginView.setAttemptFailed(Error.INTERNAL_ERROR));
    }

    private LoginView loginView;
}
