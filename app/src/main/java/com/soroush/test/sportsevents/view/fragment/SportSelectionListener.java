package com.soroush.test.sportsevents.view.fragment;

import com.soroush.test.sportsevents.data.model.Sport;

public interface SportSelectionListener {
    public void onSportSelected(Sport sport);
}
