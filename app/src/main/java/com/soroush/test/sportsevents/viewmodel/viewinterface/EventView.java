package com.soroush.test.sportsevents.viewmodel.viewinterface;

import com.soroush.test.sportsevents.data.model.Event;

import java.util.List;

public interface EventView {
    void setEvents(List<Event> events);
}
