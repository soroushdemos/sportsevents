package com.soroush.test.sportsevents.view.adapters;

public interface EventFeedbackListener {
    void onVideoLinkClicked(String url);
}
