package com.soroush.test.sportsevents.viewmodel.viewinterface;

import com.soroush.test.sportsevents.data.model.Sport;

import java.util.ArrayList;

public interface SportView {
    void setSports(ArrayList<Sport> sports);
}
