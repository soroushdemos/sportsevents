package com.soroush.test.sportsevents.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.soroush.test.sportsevents.R;
import com.soroush.test.sportsevents.data.model.Event;
import com.soroush.test.sportsevents.helper.Formatter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventRecyclerViewAdapter extends RecyclerView.Adapter<EventViewHolder> {
    public EventRecyclerViewAdapter(EventFeedbackListener listener) {
        this.listener = listener;
    }

    public void setData(List<Event> events, Map<Integer, String> teamLogos) {
        if (events != null && teamLogos != null) {
            this.events = events;
            this.teamLogos = teamLogos;
            notifyDataSetChanged();
        }
    }

    /********************************
     * RecyclerView.Adapter Methods
     ********************************/
    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EventViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.viewholder_event, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder holder, int position) {
        ImageView imageView_video = holder.itemView.findViewById(R.id.imageView_video);
        ImageView imageView_event_thumb = holder.itemView.findViewById(R.id.imageView_event_thumb);
        TextView textview_time = holder.itemView.findViewById(R.id.textView_event_time);
        TextView textView_home_title = holder.itemView.findViewById(R.id.textView_home_title);
        ImageView imageView_home_logo = holder.itemView.findViewById(R.id.imageView_home_logo);
        TextView textView_home_score = holder.itemView.findViewById(R.id.textView_home_score);
        TextView textView_away_title = holder.itemView.findViewById(R.id.textView_away_title);
        ImageView imageView_away_logo = holder.itemView.findViewById(R.id.imageView_away_logo);
        TextView textView_away_score = holder.itemView.findViewById(R.id.textView_away_score);
        final Event event = events.get(position);

        // Time
        textview_time.setText(Formatter.formatDateAndTime(
                event.dateEvent != null ? event.dateEvent : "",
                event.strTime != null ? event.strTime : ""));

        // Video Link
        if (event.strVideo != null && event.strVideo.toLowerCase().contains("youtube")) {
            imageView_video.setOnClickListener(v -> listener.onVideoLinkClicked(event.strVideo));
            imageView_video.setVisibility(View.VISIBLE);
        } else {
            imageView_video.setVisibility(View.GONE);
        }

        // HOME
        final boolean homeIsWinning = event.intHomeScore != null && event.intAwayScore != null && event.intHomeScore > event.intAwayScore;
        final boolean awayIsWinning = event.intHomeScore != null && event.intAwayScore != null && event.intHomeScore < event.intAwayScore;

        if (event.strThumb != null && !event.strThumb.isEmpty()) {
            final int size = (int) holder.itemView.getContext().getResources().getDimension(R.dimen.league_logo_height);
            Picasso.get()
                    .load(String.format("%s/preview", event.strThumb))
                    .placeholder(R.drawable.ic_image)
                    .error(R.drawable.ic_image)
                    .resizeDimen(size, size)
                    .centerInside()
                    .into(imageView_event_thumb);
            imageView_event_thumb.setVisibility(View.VISIBLE);
        } else {
            imageView_event_thumb.setVisibility(View.INVISIBLE);
        }

        int textColor = holder.itemView.getContext().getResources().getColor(
                homeIsWinning ? R.color.accentColor : R.color.primaryDark
        );
        textView_home_title.setText(event.strHomeTeam);
        textView_home_title.setTextColor(textColor);
        textView_home_score.setText(event.intHomeScore != null ? event.intHomeScore.toString() : "-");
        textView_home_score.setTextColor(textColor);

        if (teamLogos != null && teamLogos.get(event.idHomeTeam) != null && !teamLogos.get(event.idHomeTeam).isEmpty()) {
            Picasso.get()
                    .load(teamLogos.get(event.idHomeTeam))
                    .placeholder(R.drawable.ic_image)
                    .error(R.drawable.ic_image)
                    .fit()
                    .into(imageView_home_logo);
            imageView_home_logo.setVisibility(View.VISIBLE);
        } else {
            imageView_home_logo.setVisibility(View.INVISIBLE);
        }


        // AWAY
        textColor = holder.itemView.getContext().getResources().getColor(
                awayIsWinning ? R.color.accentColor : R.color.primaryDark
        );
        textView_away_title.setText(event.strAwayTeam);
        textView_away_title.setTextColor(textColor);
        if (teamLogos != null && teamLogos.get(event.idAwayTeam) != null && !teamLogos.get(event.idAwayTeam).isEmpty()) {
            Picasso.get()
                    .load(teamLogos.get(event.idAwayTeam))
                    .placeholder(R.drawable.ic_image)
                    .error(R.drawable.ic_image)
                    .fit()
                    .into(imageView_away_logo);
            imageView_away_logo.setVisibility(View.VISIBLE);
        } else {
            imageView_away_logo.setVisibility(View.INVISIBLE);
        }

        textView_away_score.setText(event.intAwayScore != null ? event.intAwayScore.toString() : "-");
        textView_away_score.setTextColor(textColor);
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    /***********
     * Private
     ***********/
    private List<Event> events = new ArrayList<>();
    private Map<Integer, String> teamLogos = new HashMap<>();
    private EventFeedbackListener listener;
}


class EventViewHolder extends RecyclerView.ViewHolder {
    EventViewHolder(@NonNull View itemView) {
        super(itemView);
    }
}