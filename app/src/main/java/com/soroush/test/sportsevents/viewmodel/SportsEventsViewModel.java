package com.soroush.test.sportsevents.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.soroush.test.sportsevents.data.actions.EventActions;
import com.soroush.test.sportsevents.data.actions.LeagueActions;
import com.soroush.test.sportsevents.data.actions.SportActions;
import com.soroush.test.sportsevents.data.actions.UserActons;
import com.soroush.test.sportsevents.data.Prefs;
import com.soroush.test.sportsevents.data.model.Event;
import com.soroush.test.sportsevents.data.model.League;
import com.soroush.test.sportsevents.data.model.Sport;
import com.soroush.test.sportsevents.data.model.Team;
import com.soroush.test.sportsevents.data.model.User;
import com.soroush.test.sportsevents.viewmodel.viewinterface.EventView;
import com.soroush.test.sportsevents.viewmodel.viewinterface.LeagueView;
import com.soroush.test.sportsevents.viewmodel.viewinterface.SportView;
import com.soroush.test.sportsevents.viewmodel.viewinterface.UserView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SportsEventsViewModel extends ViewModel implements UserView, SportView, LeagueView, EventView {
    /// Constructor
    public SportsEventsViewModel() {
        userActions = new UserActons(this);
        SportActions sportActions = new SportActions(this);
        eventActions = new EventActions(this);
        leagueActions = new LeagueActions(this);

        setUserLoggedIn(Prefs.getInstance().getIsLoggedIn());
        sportActions.fetchSports();
        eventActions.updateEventsPeriodically(selectedLeague.getValue());
    }

    /// User
    public LiveData<Boolean> getIsUserLoggedIn() {
        return isUserLoggedIn;
    }

    public void setUserLoggedIn(boolean isLoggedIn) {
        isUserLoggedIn.setValue(isLoggedIn);
        if (isLoggedIn) userActions.fetchUser();
    }

    public void logOut() {
        isUserLoggedIn.setValue(false);
        Prefs.getInstance().setIsLoggedIn(false);
        Prefs.getInstance().setCurrentUserEmail(null);
        setUser(null);
    }

    @Override
    public void setUser(User user) {
        this.user.setValue(user);
        if (user != null)
            setSelectedSportByName(user.favSport);
        else
            setSports(getSports().getValue());
    }

    /// SPORT
    public LiveData<ArrayList<Sport>> getSports() {
        return sports;
    }

    public LiveData<Sport> getSelectedSport() {
        return selectedSport;
    }

    public void setSelectedSport(Sport sport) {
        if (selectedSport.getValue() == null || selectedSport.getValue().idSport != sport.idSport) {
            updateSelectedSport(sport);
            userActions.updateUserFavSport(user.getValue(), sport.strSport);
        }
    }

    @Override
    public void setSports(ArrayList<Sport> sports) {
        this.sports.setValue(sports);
        String favSport = user.getValue() != null ? user.getValue().favSport : null;
        if (favSport == null)
            this.setSelectedSport(sports.get(0));
        else
            this.setSelectedSportByName(favSport);
    }

    /// LEAGUE
    public LiveData<ArrayList<League>> getLeagues() {
        return leagues;
    }

    public LiveData<League> getSelectedLeague() {
        return selectedLeague;
    }

    public void setSelectedLeague(League league) {
        if (selectedLeague.getValue() == null || selectedLeague.getValue().idLeague != league.idLeague) {
            events.setValue(null);
            selectedLeague.setValue(league);

            leagueActions.getTeamsForLeague(league.strLeague);
            eventActions.getEventsForLeague(league.idLeague);
        }
    }

    public LiveData<Map<Integer, String>> getTeamLogos() {
        return teamLogos;
    }

    @Override
    public void setLeagues(ArrayList<League> leagues) {
        this.leagues.setValue(leagues);
        leagueActions.populateLeagueDetails(leagues);
    }

    @Override
    public void updateLeagueDetail(League league) {
        final ArrayList<League> _leagues = new ArrayList<>();
        for (League l : getLeagues().getValue()) {
            if (l.idLeague == league.idLeague) _leagues.add(league);
            else _leagues.add(l);
        }
        leagues.setValue(_leagues);
    }

    @Override
    public void setLeagueTeams(List<Team> teams) {
        this.leagueTeams.setValue(teams);
    }

    @Override
    public void setTeamLogos(Map<Integer, String> logoMap) {
        this.teamLogos.setValue(logoMap);
    }

    /// EVENTS
    public LiveData<List<Event>> getEvents() {
        return events;
    }

    @Override
    public void setEvents(List<Event> events) {
        this.events.setValue(events);
    }

    /***********
     * Private
     ***********/
    /// Sport
    private void setSelectedSportByName(String sportName) {
        List<Sport> sports = this.sports.getValue();
        if (sports != null) {
            for (Sport sport : sports) {
                if (sport.strSport.equals(sportName)) {
                    updateSelectedSport(sport);
                    return;
                }
            }
        }
    }

    private void updateSelectedSport(Sport sport) {
        selectedSport.setValue(sport);
        leagues.setValue(null);
        events.setValue(null);
        leagueActions.getLeaguesForSportName(sport.strSport);
    }

    /// Actions
    private UserActons userActions;
    private LeagueActions leagueActions;
    private EventActions eventActions;

    /// Live Data
    private final MutableLiveData<Boolean> isUserLoggedIn = new MutableLiveData<>();
    private final MutableLiveData<User> user = new MutableLiveData<>();

    private final MutableLiveData<ArrayList<Sport>> sports = new MutableLiveData<>();
    private final MutableLiveData<Sport> selectedSport = new MutableLiveData<>();

    private final MutableLiveData<ArrayList<League>> leagues = new MutableLiveData<>();
    private final MutableLiveData<League> selectedLeague = new MutableLiveData<>();
    private final MutableLiveData<List<Team>> leagueTeams = new MutableLiveData<>();
    private final MutableLiveData<Map<Integer, String>> teamLogos = new MutableLiveData<>();

    private final MutableLiveData<List<Event>> events = new MutableLiveData<>();
}