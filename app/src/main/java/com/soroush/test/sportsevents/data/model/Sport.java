package com.soroush.test.sportsevents.data.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Sport implements Serializable {
    @SerializedName("idSport") public int idSport;
    @SerializedName("strSport") public String strSport;
    @SerializedName("strFormat") public String strFormat;
    @SerializedName("strSportThumb") public String strSportThumb;
    @SerializedName("strSportDescription") public String strSportDescription;
}
