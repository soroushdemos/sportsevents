package com.soroush.test.sportsevents.helper;

import android.util.Log;

import androidx.annotation.NonNull;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Formatter {
    public static String formatDateAndTime(@NonNull String _date, @NonNull String _time) {
        _time = _time.replace("+00:00", "");
        final String inFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'";

        String dateString = String.format("%sT%sZ", _date, _time);
        SimpleDateFormat format = new SimpleDateFormat(inFormat, Locale.UK);
        try {
            Date date = format.parse(dateString);
            return new PrettyTime().format(date);
        } catch (ParseException e) {
            Log.w(TAG, String.format("failed to parse from '%s' and '%s'", _date, _time));
        }
        return dateString;
    }

    public static boolean validateEmail(String emailCandidate) {
        String regex = "^(.+)@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(emailCandidate);
        return matcher.matches();
    }

    private static final String TAG = Formatter.class.getCanonicalName();
}
