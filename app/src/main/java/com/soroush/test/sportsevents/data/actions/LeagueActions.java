package com.soroush.test.sportsevents.data.actions;

import androidx.annotation.NonNull;

import com.soroush.test.sportsevents.data.model.League;
import com.soroush.test.sportsevents.data.model.Team;
import com.soroush.test.sportsevents.data.service.LeagueService;
import com.soroush.test.sportsevents.data.service.TeamService;
import com.soroush.test.sportsevents.viewmodel.viewinterface.LeagueView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.Disposable;

public class LeagueActions extends BaseAction {
    private LeagueView leagueView;

    public LeagueActions(LeagueView leagueView) {
        this.leagueView = leagueView;
    }

    public Disposable getLeaguesForSportName(@NonNull String sportName) {
        return LeagueService.getInstance().getAllLeagues()
                .subscribe(
                        leagues -> {
                            ArrayList<League> newValues = new ArrayList<>();
                            for (League league : leagues)
                                if (league.strSport.equals(sportName))
                                    newValues.add(league);

                            leagueView.setLeagues(newValues);
                            if (newValues.size() > 0) leagueView.setSelectedLeague(newValues.get(0));
                        },
                        this::logError
                );

    }

    public void populateLeagueDetails(List<League> leagues) {
        for (League league : leagues) {
            final Disposable d = LeagueService.getInstance().getLeagueDetail(league.idLeague)
                    .subscribe(
                            leagueView::updateLeagueDetail,
                            this::logError
                    );
        }
    }

    public Disposable getTeamsForLeague(@NonNull String leagueName) {
        return TeamService.getInstance().getTeamsByLeagueName(leagueName)
                .subscribe(
                        teams -> {
                            leagueView.setLeagueTeams(teams);
                            Map<Integer, String> teamLogoMap = new HashMap<>();
                            for (Team team : teams) teamLogoMap.put(team.idTeam, team.strTeamBadge);

                            leagueView.setTeamLogos(teamLogoMap);
                        },
                        this::logError
                );

    }
}
