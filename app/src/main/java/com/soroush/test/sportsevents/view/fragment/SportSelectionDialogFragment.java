package com.soroush.test.sportsevents.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.soroush.test.sportsevents.R;
import com.soroush.test.sportsevents.data.model.Sport;
import com.soroush.test.sportsevents.view.adapters.SportRecyclerViewAdapter;

import java.util.ArrayList;

public class SportSelectionDialogFragment extends DialogFragment {
    public static void show(FragmentManager fm, ArrayList<Sport> sports, Sport selectedSport) {
        // instantiate
        SportSelectionDialogFragment f = new SportSelectionDialogFragment();

        // build args
        Bundle args = new Bundle();
        args.putSerializable(ARG_SPORTS, sports);
        args.putSerializable(ARG_SELECTED_SPORT, selectedSport);
        f.setArguments(args);

        // show fragment (+ replace existing previous instance)
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag(TAG);
        if (prev != null) ft.remove(prev);
        ft.addToBackStack(null);
        f.show(ft, TAG);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sports = (ArrayList<Sport>) getArguments().getSerializable(ARG_SPORTS);
        selectedSport = (Sport) getArguments().getSerializable(ARG_SELECTED_SPORT);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_sport_select, container, false);
        RecyclerView recyclerView = v.findViewById(R.id.recyclerView_sports);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new SportRecyclerViewAdapter(sports, selectedSport, sport -> {
            listener.onSportSelected(sport);
            dismiss();
        }));
        return v;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof SportSelectionListener)
            this.listener = (SportSelectionListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    private ArrayList<Sport> sports = null;
    private Sport selectedSport = null;
    private SportSelectionListener listener = null;

    private static final String ARG_SPORTS = "args_sports";
    private static final String ARG_SELECTED_SPORT = "args_selected_sport";
    private static final String TAG = SportSelectionDialogFragment.class.getCanonicalName();
}

