package com.soroush.test.sportsevents.data.service;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.soroush.test.sportsevents.data.model.Sport;
import com.soroush.test.sportsevents.helper.Constants;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;

public class SportService {
    /**********
     * Public
     **********/
    @NonNull
    public static SportService getInstance() throws IllegalStateException {
        if (instance == null) throw new IllegalStateException(Constants.NOT_INSTANTIATED);
        return instance;
    }

    public Observable<ArrayList<Sport>> getAllSports() {
        return api.getAllSports()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(sportsResponse -> sportsResponse.sports);
    }

    /******************
     * Package Private
     ******************/
    static void instantiate(Context context) {
        if (instance == null) {
            instance = new SportService(context);
        }
    }

    private SportService(Context context) {
        api = Retrofit.getClient(context).create(SportsApi.class);
    }

    /**********
     * Private
     **********/
    @Nullable
    private static SportService instance;
    private SportsApi api;
}

class SportsResponse {
    ArrayList<Sport> sports;
}


interface SportsApi {
    @GET("all_sports.php")
    Observable<SportsResponse> getAllSports();
}
