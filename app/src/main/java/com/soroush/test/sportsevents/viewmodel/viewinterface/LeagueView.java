package com.soroush.test.sportsevents.viewmodel.viewinterface;

import com.soroush.test.sportsevents.data.model.League;
import com.soroush.test.sportsevents.data.model.Team;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface LeagueView {
    void setLeagues(ArrayList<League> leagues);
    void setSelectedLeague(League league);
    void updateLeagueDetail(League leage);
    void setLeagueTeams(List<Team> teams);
    void setTeamLogos(Map<Integer, String> logoMap);
}
