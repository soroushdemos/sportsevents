package com.soroush.test.sportsevents.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import static com.soroush.test.sportsevents.helper.Constants.DB_TABLE_USER;

@Entity(tableName = DB_TABLE_USER)
public class User {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "email")
    public String email;

    @ColumnInfo(name = "password")
    public String password;

    @ColumnInfo(name = "favSport")
    public String favSport;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
