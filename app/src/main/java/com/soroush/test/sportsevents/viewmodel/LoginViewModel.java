package com.soroush.test.sportsevents.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.soroush.test.sportsevents.data.Prefs;
import com.soroush.test.sportsevents.data.actions.LoginActions;
import com.soroush.test.sportsevents.viewmodel.viewinterface.LoginView;

import io.reactivex.disposables.Disposable;

public class LoginViewModel extends ViewModel implements LoginView {
    public LoginViewModel() {
        loginActions = new LoginActions(this);
    }

    @Override
    public void setAttemptStart() {
        attemptStatus.setValue(LoginActions.AttemptStatus.STARTED);
        this.error.setValue(null);
    }

    @Override
    public void setAttemptSucceeded(String email) {
        Prefs.getInstance().setIsLoggedIn(true);
        Prefs.getInstance().setCurrentUserEmail(email);
        attemptStatus.setValue(LoginActions.AttemptStatus.SUCCEEDED);
    }


    @Override
    public void setAttemptFailed(LoginActions.Error error) {
        attemptStatus.setValue(LoginActions.AttemptStatus.FAILED);
        this.error.setValue(error);
    }

    public LiveData<LoginActions.AttemptStatus> getAttemptStatus() {
        return attemptStatus;
    }

    public LiveData<LoginActions.Error> getError() {
        return error;
    }

    public Disposable attemptLogin(@NonNull String userName, @NonNull String password) {
        return loginActions.attemptLogin(userName, password);
    }

    public Disposable attemptSignUp(@NonNull String userName, @NonNull String password) {
        return loginActions.attemptSignUp(userName, password);
    }

    /***********
     * Private
     ***********/
    private LoginActions loginActions;
    private final MutableLiveData<LoginActions.AttemptStatus> attemptStatus = new MutableLiveData<>();
    private final MutableLiveData<LoginActions.Error> error = new MutableLiveData<>();
}
