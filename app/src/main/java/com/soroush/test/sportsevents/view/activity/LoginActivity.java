package com.soroush.test.sportsevents.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.soroush.test.sportsevents.R;
import com.soroush.test.sportsevents.helper.Constants;
import com.soroush.test.sportsevents.helper.Formatter;
import com.soroush.test.sportsevents.viewmodel.LoginViewModel;

public class LoginActivity extends AppCompatActivity {
    public static void startForResult(Activity activity, int requestCode) {
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        viewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        setupViews();
        setupCallbacks();
        setupViewModelObservers();
    }

    private LoginViewModel viewModel;

    private void setupViews() {
        EditText editText_userName = findViewById(R.id.editText_userName);
        editText_userName.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                final boolean isGoodEmail = Formatter.validateEmail(editText_userName.getText().toString());
                if (!isGoodEmail)
                    editText_userName.setError(getResources().getString(R.string.invalid_email));
            }
        });
    }

    private void setupCallbacks() {
        findViewById(R.id.button_login)
                .setOnClickListener(v -> viewModel.attemptLogin(getUserName(), getPassword()));

        findViewById(R.id.button_signup)
                .setOnClickListener(v -> viewModel.attemptSignUp(getUserName(), getPassword()));
    }

    private void setupViewModelObservers() {
        TextView textView_error = findViewById(R.id.textView_error);
        Button buttonLogin = findViewById(R.id.button_login);
        ProgressBar progress = findViewById(R.id.progress);
        Button buttonSignUp = findViewById(R.id.button_signup);

        viewModel.getAttemptStatus().observe(this, status -> {
            switch (status) {
                case STARTED:
                    buttonSignUp.setVisibility(View.GONE);
                    buttonLogin.setVisibility(View.GONE);
                    progress.setVisibility(View.VISIBLE);
                    break;
                case SUCCEEDED:
                    progress.setVisibility(View.GONE);
                    setResult(Constants.RESULT_SUCCESS);
                    finish();
                    break;
                case FAILED:
                    progress.setVisibility(View.GONE);
                    buttonLogin.setVisibility(View.VISIBLE);
                    buttonSignUp.setVisibility(View.VISIBLE);
                    break;
            }

        });

        viewModel.getError().observe(this, error -> {
            if (error == null) return;

            switch (error) {
                case USER_ALREADY_EXISTS:
                    textView_error.setText(String.format(
                            getResources().getString(R.string.user_already_exists),
                            getUserName()
                    ));
                    break;
                case WRONG_USERNAME_OR_PASSWORD:
                    textView_error.setText(getResources().getString(R.string.wrong_credentials));
                    break;
                case INTERNAL_ERROR:
                    textView_error.setText(getResources().getString(R.string.internal_error_message));
            }
        });
    }

    @NonNull
    private String getUserName() {
        return ((EditText) findViewById(R.id.editText_userName)).getText().toString().trim();
    }

    @NonNull
    private String getPassword() {
        return ((EditText) findViewById(R.id.editText_password)).getText().toString().trim();
    }
}
