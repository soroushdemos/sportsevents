package com.soroush.test.sportsevents.view.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.soroush.test.sportsevents.R;
import com.soroush.test.sportsevents.data.model.League;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class LeagueRecyclerViewAdapter extends RecyclerView.Adapter<LeagueViewHolder> {
    public LeagueRecyclerViewAdapter(@NonNull OnLeagueSelectedListener listener) {
        this.listener = listener;
    }

    public void setLeagues(ArrayList<League> leagues, League selectedLeague) {
        if (leagues != null && selectedLeague != null) {
            this.leagues = leagues;
            this.selectedLeague = selectedLeague;
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public LeagueViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LeagueViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.viewholder_league, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull LeagueViewHolder holder, int position) {
        final League league = leagues.get(position);
        holder.itemView.setBackgroundColor(
                holder.itemView.getContext().getResources().getColor(
                        selectedLeague != null && league.idLeague == selectedLeague.idLeague ? R.color.accentColor : R.color.backgroundColorDark
                )
        );

        holder.itemView.setOnClickListener(v -> {
            if (listener != null) listener.onLeagueSelected(league);
        });

        ((TextView) holder.itemView.findViewById(R.id.textView_league_title))
                .setText(league.strLeague);

        if (league.strLogo != null) {
            ImageView imageView = holder.itemView.findViewById(R.id.imageView_league_logo);
            Picasso.get()
                    .load(String.format("%s/preview", league.strLogo))
                    .placeholder(R.drawable.ic_image)
                    .error(R.drawable.ic_image)
                    .fit()
                    .into(imageView);
        }
    }

    @Override
    public int getItemCount() {
        return leagues.size();
    }

    /***********
     * Private
     ***********/
    private ArrayList<League> leagues = new ArrayList<>();
    @NonNull
    private League selectedLeague;
    @Nullable
    private OnLeagueSelectedListener listener;
}

class LeagueViewHolder extends RecyclerView.ViewHolder {
    LeagueViewHolder(@NonNull View itemView) {
        super(itemView);
    }
}


