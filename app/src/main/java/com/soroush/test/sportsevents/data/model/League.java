package com.soroush.test.sportsevents.data.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

public class League {
    @NonNull
    @SerializedName("idLeague")
    public int idLeague;

    @NonNull
    @SerializedName("strSport")
    public String strSport;

    @NonNull
    @SerializedName("strLeague")
    public String strLeague;

    @NonNull
    @SerializedName("strLeagueAlternate")
    public String strLeagueAlternate;


    @SerializedName("idSoccerXML") public int idSoccerXML;
    @SerializedName("idAPIfootball") public int idAPIfootball;
    @SerializedName("strDivision") public String strDivision;
    @SerializedName("idCup") public int idCup;
    @SerializedName("intFormedYear") public int intFormedYear;
    @SerializedName("dateFirstEvent") public String dateFirstEvent;
    @SerializedName("strGender") public String strGender;
    @SerializedName("strCountry") public String strCountry;
    @SerializedName("strWebsite") public String strWebsite;
    @SerializedName("strFacebook") public String strFacebook;
    @SerializedName("strTwitter") public String strTwitter;
    @SerializedName("strYoutube") public String strYoutube;
    @SerializedName("strRSS") public String strRSS;
    @SerializedName("strDescriptionEN") public String strDescriptionEN;
    @SerializedName("strDescriptionDE") public String strDescriptionDE;
    @SerializedName("strDescriptionFR") public String strDescriptionFR;
    @SerializedName("strDescriptionIT") public String strDescriptionIT;
    @SerializedName("strDescriptionCN") public String strDescriptionCN;
    @SerializedName("strDescriptionJP") public String strDescriptionJP;
    @SerializedName("strDescriptionRU") public String strDescriptionRU;
    @SerializedName("strDescriptionES") public String strDescriptionES;
    @SerializedName("strDescriptionPT") public String strDescriptionPT;
    @SerializedName("strDescriptionSE") public String strDescriptionSE;
    @SerializedName("strDescriptionNL") public String strDescriptionNL;
    @SerializedName("strDescriptionHU") public String strDescriptionHU;
    @SerializedName("strDescriptionNO") public String strDescriptionNO;
    @SerializedName("strDescriptionPL") public String strDescriptionPL;
    @SerializedName("strDescriptionIL") public String strDescriptionIL;
    @SerializedName("strFanart1") public String strFanart1;
    @SerializedName("strFanart2") public String strFanart2;
    @SerializedName("strFanart3") public String strFanart3;
    @SerializedName("strFanart4") public String strFanart4;
    @SerializedName("strBanner") public String strBanner;
    @SerializedName("strBadge") public String strBadge;

    @Nullable @SerializedName("strLogo") public String strLogo;
    @SerializedName("strPoster") public String strPoster;
    @SerializedName("strTrophy") public String strTrophy;
    @SerializedName("strNaming") public String strNaming;
    @SerializedName("strComplete") public String strComplete;
    @SerializedName("strLocked") public String strLocked;
}
