package com.soroush.test.sportsevents.helper;

import java.util.Arrays;
import java.util.List;

public final class Constants {
    /*****************
     * Config
     *****************/
    public static final String PREFS_NAME = "sportevets_prefs";
    public static final String DB_NAME = "sportevets_db_production";
    public static final int UPDATE_INTERVAL = 10;

    public static final List<String> SUPPORTED_SPORT_NAMES = Arrays.asList(
            "Soccer", "Ice Hockey", "Basketball", "Rugby", "Australian Football",
            "Esports", "Darts", "Snooker", "Handball", "ESports", "Field Hockey",
            "Volleyball", "Netball", "Cricket", "Tennis", "American Football", "Baseball"
    );

    /*****************
     * Error Messages
     *****************/
    public static final String NOT_INSTANTIATED = "Service has not been instantiated... call instantiate(Context context) first";
    public static final String CANNOT_BE_EMPTY = "'%s' cannot be empty!";

    /*****************
     * Request Codes
     *****************/
    public static final int ACTIVITY_REQUEST_LOGIN = 0;
    public static final int RESULT_SUCCESS = 1;

    /*****************
     * DB Table Names
     *****************/
    public static final String DB_TABLE_USER = "user";
}
