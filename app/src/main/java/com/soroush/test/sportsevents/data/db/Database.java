package com.soroush.test.sportsevents.data.db;

import androidx.room.RoomDatabase;

import com.soroush.test.sportsevents.data.db.dao.UserDao;
import com.soroush.test.sportsevents.data.model.User;

@androidx.room.Database(entities = {User.class}, version = 1)
public abstract class Database extends RoomDatabase {
    public abstract UserDao userDao();
}
