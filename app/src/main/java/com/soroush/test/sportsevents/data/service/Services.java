package com.soroush.test.sportsevents.data.service;

import android.content.Context;

public final class Services {

    public static void instantiate(Context context) {
        EventsService.instantiate(context);
        LeagueService.instantiate(context);
        TeamService.instantiate(context);
        SportService.instantiate(context);
    }
}
