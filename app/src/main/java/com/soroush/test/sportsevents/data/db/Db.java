package com.soroush.test.sportsevents.data.db;

import android.content.Context;

import androidx.room.Room;

import com.soroush.test.sportsevents.helper.Constants;

public class Db {
    public static Database getInstance() throws IllegalStateException {
        if (instance == null) throw new IllegalStateException(Constants.NOT_INSTANTIATED);
        return instance;
    }

    public static void instantiate(Context context) {
        instance = Room.databaseBuilder(context, Database.class, Constants.DB_NAME).build();
    }

    private static Database instance;
}
