package com.soroush.test.sportsevents.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.soroush.test.sportsevents.R;
import com.soroush.test.sportsevents.data.model.Sport;
import com.soroush.test.sportsevents.view.fragment.SportSelectionListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SportRecyclerViewAdapter extends RecyclerView.Adapter<SportViewHolder> {
    public SportRecyclerViewAdapter(ArrayList<Sport> sports, Sport selectedSport, SportSelectionListener listener) {
        this.sports = sports;
        this.selectedSport = selectedSport;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SportViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.viewholder_sport, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull SportViewHolder holder, int position) {
        final Sport sport = sports.get(position);

        TextView title = holder.itemView.findViewById(R.id.textView_sport_title);
        ImageView image = holder.itemView.findViewById(R.id.imageView_sport_logo);
        ImageView check = holder.itemView.findViewById(R.id.imageView_selected);

        Picasso.get()
                .load(sport.strSportThumb)
                .placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_image)
                .fit()
                .into(image);

        title.setText(sport.strSport);

        final boolean isSelected = sport.idSport == selectedSport.idSport;
        title.setTextColor(holder.itemView.getContext().getResources().getColor(
             isSelected ? R.color.accentColor : R.color.primary
        ));
        check.setVisibility(isSelected ? View.VISIBLE : View.GONE);

        image.setOnClickListener(v -> {
            selectedSport = sport;
            notifyDataSetChanged();
            if (listener != null)
                listener.onSportSelected(sport);
        });
    }

    @Override
    public int getItemCount() {
        return sports.size();
    }

    private ArrayList<Sport> sports;
    private Sport selectedSport;
    private SportSelectionListener listener;
}


class SportViewHolder extends RecyclerView.ViewHolder {
    SportViewHolder(@NonNull View itemView) {
        super(itemView);
    }
}