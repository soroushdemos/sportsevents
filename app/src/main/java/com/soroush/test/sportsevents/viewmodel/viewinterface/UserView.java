package com.soroush.test.sportsevents.viewmodel.viewinterface;

import com.soroush.test.sportsevents.data.model.User;

public interface UserView {
    void setUser(User user);
}
